typedef bool int;
typedef str char*;
#define true 1
#define false 0


bool checkPassword(str in, str pw)
{
    int len = min(in.len, pw.len);

    for (int i = 0; i < len; i++)
        if (in[i] != pw[i])
            return false;

    return true;
}

bool checkPassword(str in, str pw)
{
    char x = 0;
    int len = min(in.len, pw.len);
    
    for (int i = 0; i < len; i++)
        x |= in[i] ^ pw[i];

    return (x == 0);
}
