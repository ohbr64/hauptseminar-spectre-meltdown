# Referenzen

## Spectre 

CVE-2017-5753
CVE-2017-5715

## Security updates

Kernel updates (Q2):
kernel-3.10.0-693.5.2.el7.x86 64 was updated with kernel-3.10.0-693.11.6.el7.x86 64 which fixes CVE-2017-5753, CVE2017-5715 and CVE-2017-5754

degradation (Q2):
- parallel single node computation: 2-3%
- parallel dual node computation: 5-11%
- local file system, meta data: 10-20%
- local file system, read & write operations: 0-3%

## Volunerable instructions (Q5)

RISC Instructions (CHERI) 

## Volunerable (Q5)

Variant 1: bounds-check bypass (CVE-2017-5753) Published by Koch et al., this vulnerability allows the attacker to manipulate speculation such that software invariants (e.g., array bounds checks) are violated in speculation, with their side effects (e.g., out-ofbounds loads and resulting computations) being detectable using cache side channels. The authors demonstrate an attack in which a speculated out-of-bounds array access can be triggered, allowing the contents of arbitrary kernel memory to be leaked via timing. We observe that while bypassing bounds checks is a particularly catastrophic outcome of the underlying vulnerability, there might be other potential non-bounds-check implications of security significance – e.g., bypassing kernel access-control checks dependent on uncached data.

Variant 2: branch target injection (CVE-2017-5715) Published by Koch et al., this vulnerability relies on leakage of branch-predictor state between contexts, allowing an attacker to train the branch predictor such that they can maliciously influence speculated control flow in another security domain in order to leak data accessible in that domain back to the attacker via a cache timing side channel. The authors demonstrate an virtualizationbased attack in which a guest operating system can guide speculated instructions in the host to access known addresses based on private information, allowing the contents of arbitrary host memory to be leaked via timing.

Variant 3: rogue data cache load (CVE-2017-5754) Published by Lipp et al., this vulnerability relies on speculative loads being performed before hardware permission checks (e.g., based on page permissions or ring), allowing architecturally inaccessible data to 7 be revealed through cache timing. The authors demonstrate arbitrary kernel memory extraction from user space at hundreds of kilobytes per second.

## Side-Channel Attack Beispiel (Q6 Seite 2)

