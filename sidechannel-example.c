int checkPassword(char *input, char *password, int len)
{
	for (int i = 0; i < len; i++)
		if(input[i] != password[i])
			return 0;
	return 1;
}

int checkPassword(char *input, char *password, int len)
{
	int x = 0;
	for(int i = 0; i < len; i++)
		x |= input[i] ^ password[i];
	return !x;	
}