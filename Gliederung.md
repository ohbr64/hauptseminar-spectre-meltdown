# Einfuhrung Out-Of-Order-Execution

- Ausführung von Instruktionen in anderer Reihenfolge
- Sind Instruktionen nicht voneinander abhängig so kann der Prozessor Instruktion, die im Fluss der Instruktionen eigentlich erst später vorkommen, parallel oder sogar vor vorherigen Instruktionen ausführen.
- Dies erlaubt effizientere Nutzung der verschiedenen CPU Komponenten und vermindert "Wartezeiten" immens.
- Durchgeführte Instruktionen werden in einen sogenannten Sortier-Buffer geschrieben und erst committed, wenn alle voherigen Instruktionen ausgeführt wurden. So kann nach außen das Einhalten der Reihenfolge vorgetäuscht werden.

# Einführung in Speculative Execution

- Oftmals weiß der Prozessor nicht, welche Instruktionen als nächstes folgen. Dies ist z.B. bei einer Verzweigung der Fall
- Je nach Bedingung geht die Ausführung in eine andere Richtung weiter
- In solchen Fällen kann der Prozessor einen Speicherpunkt festlegen in dem er seine Registerinhalte zurücklegt
- Nun wird er das Ergebnis der Bedingung "erraten" und während der Berechnung den erwarteten Weg *spekulativ* weiterlaufen
- Hat er richtig geraten wird der Checkpoint verworfen
- Hat er falsch geraten wird die spekulative Ausführung verworfen und abgebrochen und der Status des Checkpoints neugeladen
- In beiden Fällen geht nun die Ausführung normal weiter
- Das verwerfen der bereits ausgeführten falschen Instruktionen ist für das Programm nicht sichtbar, um den logischen Zustand des Programms nach außen zu wahren, als wäre man stets dem richtigen Zweig gefolgt

## Zweigvorhersage

- Bei der spekulativen Ausführung kann der Prozessor nicht magisch den richtigen Pfad erraten
- Eine Methode zur Bestimmung des wahrscheinlichsten Ergebnisses ist erforderlich
- Hierfür haben moderne Prozessoren mehrere Hardwarekomponenten unter anderem den Branch Target Buffer(BTB)
- Der BTB speichert eine Map von kürzlich ausgeführten Sprunginstruktionen und deren Zieladresse
- So kann der Prozessor mit dem BTB die mögliche Zieladresse schon vor dekodieren der eigentlichen Sprunginstruktion voraussagen.
- Für bedingte Sprünge(Verzweigungen) reicht dies noch nicht
- Um zu bestimmen ob/wie eine Verzweigung ausgeführt wird muss der Prozessor zusätzlich die letzten Ergebnisse von Verzweigungsbedingungen aufzeichnen.

# Einführung in die Speicher-Hierarchie

- Bis heute herrscht eine große Lücke zwischen Speicher- und Prozessorgeschwindigkeit. Wartet der Prozessor, um einen Wert aus dem Hauptspeicher(RAM) auszulesen können Hunderte, wenn nicht mehr, Zyklen vergehen
    - Grafik für L1, L2, L3, RAM, SSD, HDD, Network, ...
- Um diese Verschwendung von Prozessorzyklen zu mindern nutzt der Prozessor eine Hierarchie von immer kleineren, dafür deutlich schnelleren Caches.
- Caches teilen Speicher in Blöcke fester Größe(Linien) auf. Diese sind i.d.R in der Größenordnung von 64 oder 128 Bytes
- Benötigt der Prozessor Daten aus dem Speicher wird zunächst geprüft, ob die benötigten Daten im L1(Level 1) Cache vorhanden sind
- Ist dies der Fall ist die Anfrage ein Cache Treffer und die Daten können sehr schnell verarbeitet werden
- Im anderen Fall ist die Anfrage ein Cache Fehlschlag, nun wird die Prozedur mit dem nächsten Cache Level wiederholt
- Des Weiteren werden die Daten im Falle eines Erfolgs im L1 Cache abgelegt, falls sie in naher Zukunft wieder benötigt werden
- Moderne Prozessoren haben typischerweise drei Cache Level. Jeder Kern hat seinen eigenen L1 und L2 Cache und einen von allen Kernen geteilten L3 Cache, auch bekannt als Last-Level-Cache(LLC)
- Sind die Daten in keinem Cache enthalten müssen sie natürlich trotzdem aus dem RAM geladen werden. Statistisch ist diese Prozedur trotzdem wesentlich effizienter als alles immer aus dem RAM laden zu müssen. Viele Cache Treffer führen zu wesentlich schnellerer Ausführung

# Einführung Side-Channel-Attacks

- Entnehmen sensibler Informationen via "Side"-Channel, da nicht vorgesehener In/Out-put
- Konventionelle Angriffe -> Z.B. Interne Logikfehler
- Side-Channel Angriffe -> Z.B. Responsetime des Systems
- Sensible Informationen = Side-Channel Leakage
- Viele Arten des Leakens 
    - Responsetime
    - Änderung der elektrischen Aktivität in Schaltungen
    - Elektromagnetische Emissionen
    - Ton eines Kryptographischen Geräts
        - Klicken des Mechanismus in einem Safe -> Side-Channel Leakage
- Neben Beobachten kann ein Angreifer den Input gezielt manipulieren, um dem System noch mehr Informationen zu entlocken

## Beispiel:
- System zur Verifizierung von Passwörtern vergleicht Zeichen für Zeichen
- Input aller möglichen Symbole
- Das System stellt beim ersten Zeichen eine Ungleichheit fest und bricht ab
- Ist das erste Zeichen richtig bricht das System erst beim zweiten Zeichen ab
- Etwas längere Responsetime = Erstes Zeichen war richtig.
- So könnte man Zeichen um Zeichen das richtige Passwort erraten 
- Bei einem 10-stelligen alphanumerischen Passwort maximal: 62*10(620) statt 62^10(839299365868340200) Versuche nötig

Code-Beispiel Quelle 06

## Was man für einen Side-Channel Angriff braucht:

1. Einen Side Channel, geteilte Resource zwischen Angreifer und Opfer (zuvor genannte Beispiele)
2. Manipulation der Resource basierend auf sensiblen(geheimen) Daten 
    - Beispiel: Verzweigung (if/else) hängt von Geheimdaten ab und manipuliert je nach Zweig den Side-Channel anders
3. Messgenauigkeit und Menge an Daten. 

- Eine geteilte Resource kann von anderen Operationen ohne weiteren Zusammenhang ebenfalls verändert werden. Angreifer muss mit genug Genauigkeit messen können um nützliche Informationen zu leaken.
- Das Beispiel Passwort-Vergleichen funktioniert in einem isolierten System gut
- In einer größeren Anwendung(Datenbank, Webserver etc.) können andere Responsetimes die unterschiedlichsten Ursachen haben(Verbindung aufbauen, Auslastung der Anwendung durch andere Anfragen etc.)
- Solche Messungenauigkeit(en) zwingen einen Angreifer mehr Messungen durchzuführen -> erhöhte Chance entdeckt zu werden

## Strategien zur Verhinderung/Minderung

- Zum Verhindern eines Side-Channel Angriffs ist es am einfachsten oben genannte Vorraussetzungen nicht zu erfüllen

1. Entfernen von unterscheidbarem Verhalten basierend auf sensiblen Daten
    - Immer beide Zweige berechnen
    - Sicherstellen, dass beide Zweige diesselbe Auswirkung auf Side-Channel(Cache oder Speicheradressen) haben
    - Ausführung immer eine feste Zeit laufen lassen, auch wenn währenddessen keine nützliche Arbeit erbracht wird oder die Berechnung noch nicht beendet ist

2. Verstecken des gesuchten Side-Channel Verhalten
    - Die meisten Side-Channel Angriffe basieren auf einer genauen Analyse des Opfers. Dem Angreifer Zugriff auf die Binary zu verhindern ist, wenn auch nicht die beste, eine Möglichkeit.
    - Zufälliges Kompilieren des Codes oder Ändern der Flusskontrolle wäre eine valide, wenn auch komplizierte, Verteidigungsstrategie
    - Strategien wie ASLR(address space layout randomization) sind zu allgemein, um Side-Channel zu verhindern

3. Selbst Side-Channel Leakage messen. Angriffe auf Side-Channel von außen haben meist auch Einfluss auf das Opfer. Im vorigen Beispiel das Cache Verhalten des Opfers. 
    - Messen der eigenen Ausführungszeit, bei Verdacht auf einen Angriff -> Ausführung abbrechen

# Problematik bei Out-Of-Order-Execution (Meltdown)

- Intel only
- KAISER patch

Exploited Vulnerability | CVE | Exploit Name | Public Vulnerability Name
---|---|---|---
Meltdown | 2017-5754 | Variant 3 | Rogue Data Cache Load

# Problematik der Speculative Execution (Spectre)

- Intel, AMD, ARM


Exploited Vulnerability | CVE | Exploit Name | Public Vulnerability Name
---|---|---|---
Spectre | 2017-5753 | Variant 1 | Bounds Check Bypass
Spectre | 2017-5715 | Variant 2 | Branch Target Injection

# Auswirkungen für professionele Anwendungen
# Auswirkungen für den Endnutzer
## BIOS
## Windoof-patch-abfuck
## Linux
# Mügliche Lüsungen
## Neue Prozessorarchitekturen
## CHERI
# Ausblick
